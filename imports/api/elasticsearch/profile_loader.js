"use strict";

const fs = require('fs');
const es = require('elasticsearch');
const path = require('path');

let client = new es.Client({
	host:'localhost:9200'
});

let import_json = function(){
	console.log(path.resolve());
	let actual_path = path.resolve();
	let json_path = path.join(actual_path, './imports/api/elasticsearch/profiles.json');
	console.log('json_path', json_path);
	fs.readFile(json_path, 'utf-8', (err, data) => {
		if (err) {
			throw err; 
		}

		let id_count = 1;
		let bulk_request = data
			.split('\n')
			.reduce((bulk_request, line) => {
				let obj;
				try{
					obj = JSON.parse(line);
					obj.id = id_count;
				} catch(e){
					console.log('Done reading');
					return bulk_request;
				}

				bulk_request.push({index: {_index: 'profiles', _type: 'profile', _id: id_count}});
				bulk_request.push(obj);
				id_count += 1;
				return bulk_request;
			}, []);

		let insert_on_elastic = function(){
			client.bulk({
				body: bulk_request
			}, function (err, resp){
				if (err) {
					console.log(err);
				}
				console.log('Inserted all records');
			});
		};

		insert_on_elastic();
	});	
}

module.exports = import_json
// import_json();
