# Elasticsearch-POC #

This repositrory is a POC (Proof of concept) to use meteor with elasticsearch over a simple search engine.


### How do I get set up? ###

* Download [MeteorJS] 
* Download [Elasticsearch] 
* execute ```npm install``` on the project

### To populate the elasticsearch base ###
* first initialize your elasticsearch:
execute ```bin/elasticsearch``` on the folder that you have installed elasticsearch
* Then in project root, execute:
```node```, to start node console and:
```const profile_loader = require('../imports/api/elasticsearch/profile_loader.js');```
```profile_loader()```
* If everything goes fine, you'll receive the message "Inserted all records" on the console.

### Execute locally ###
```meteor run```


That's all for now :/, sorry..

[MeteorJS]: <https://www.meteor.com/≥
[Elasticsearch]: <https://www.elastic.co/downloads/elasticsearch>